<?php
## Database Connection...
include_once('../config/connection.php');

$_POST = json_decode(file_get_contents('php://input'), true);

$emp_name = $_POST['emp_name'];
$emp_depart = $_POST['emp_depart'];
$emp_email = $_POST['emp_email'];
$emp_id = $_POST['emp_id'];
$sal_month = $_POST['sal_month'];
$basic = $_POST['basic'];
$other_ben = $_POST['other_ben'];
$gross_sal = $_POST['gross_sal'];
$paye = $_POST['paye'];
$p_relief = $_POST['p_relief'];
$paye_bal = $_POST['paye_bal'];
$nssf = $_POST['nssf'];
$nhif = $_POST['nhif'];
$total_deduct = $_POST['total_deduct'];
$net_pay = $_POST['net_pay'];

$q = "INSERT INTO payslips (user_id, names, email, department, sal_date, basic, other, gross, paye, p_relief, net_paye, nssf, nhif, total_deduct, net_pay) VALUES ('$emp_id', '$emp_name', '$emp_email', '$emp_depart', '$sal_month', '$basic', '$other_ben', '$gross_sal', '$paye', '$p_relief', '$paye_bal', '$nssf', '$nhif', '$total_deduct', '$net_pay')";

$r = mysqli_query($dbc, $q);

if ($r) {
	$date = date_create($sal_month);
	$results['status'] = "success";
	$results['message'] = $emp_name . " Has been paid";
}else{
	$results['status'] = "error";
	$results['message'] = "Employee could not be paid.";
}

header('Content-type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);


?>