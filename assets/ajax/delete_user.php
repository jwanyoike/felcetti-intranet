<?php
## Database Connection...
include_once('../config/connection.php');
$_POST = json_decode(file_get_contents('php://input'), true);
## Database Fetch Functions...
// include_once('../functions/data.php');

$id = $_POST['id'];

$q = "DELETE FROM users WHERE id = $id";
$r = mysqli_query($dbc, $q);

if ($r) {
	$results['status'] = "success";
	$results['message'] = "The user was deleted successfully.";
}else{
	$results['status'] = "error";
	$results['message'] = "The user could not be deleted.";
}

header('Content-type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);
?>
