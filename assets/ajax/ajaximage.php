<?php
include_once('../config/connection.php');
## Database Fetch Functions...
include_once('../functions/data.php');

// $_POST = json_decode(file_get_contents('php://input'), true);

$name = $_FILES['photoimg']['name'];
$size = $_FILES['photoimg']['size'];
$user_id = $_POST['user_id'];
$avatar = $_POST['avatar'];
$user_email = $_SESSION['uid'];

$sizes = array(128 => 128);
$valid_formats = array("jpeg", "jpg", "png", "gif", "bmp");

if (strlen($name)) {
	list($txt, $ext) = explode(".", $name);
	if(in_array( strtolower($ext), $valid_formats)){
		if($size<(1024*1024)){
			$ext = strtolower(pathinfo($_FILES['photoimg']['name'], PATHINFO_EXTENSION));
			foreach ($sizes as $width => $height) {
		        $file_extn = strtolower(end(explode('.', $name)));
		        $file_temp = $_FILES['photoimg']['tmp_name'];
		        list($w, $h) = getimagesize($file_temp);
		        $ratio = max($width/$w, $height/$h);
		        $h = ceil($height / $ratio);
		      	$x = ($w - $width / $ratio) / 2;
		      	$w = ceil($width / $ratio);
		      	
		        $path = "../images/users/";
		      	$new_image_name = substr(md5(time()), 0, 10) . '.' . $file_extn;

		        /* read binary data from image file */
		      	$imgString = file_get_contents($file_temp);
		      	/* create image from string */
		      	$image = imagecreatefromstring($imgString);
		      	$tmp = imagecreatetruecolor($width, $height);
		      	imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
		        imagejpeg($tmp, $path.$new_image_name, 100);

		        $remover = unlink($path.$avatar);

		        $q = "UPDATE users SET avatar = '$new_image_name' WHERE id = '$user_id'";
				$r = mysqli_query($dbc, $q);

				if ($r) {
					 $results = array("status"=>"success", "message"=> "<img src='../assets/images/users/".$new_image_name."' alt='".$user_id."' class='img-responsive img-rounded'>");
				}else {
					$results = array("status"=>"error", "message"=>"Please try again");
				}
			}
		}else {
			$results = array("status"=>"error", "message"=>"Image file size max 1 MB");
		}
	}else {
		$results = array("status"=>"error", "message"=>"Invalid file format..");
	}

}else {
	$results = array("status"=>"error", "message"=>"try again.");
}
// $results = array("status"=>"success", "message"=>$user_id);
header('Content-type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);
?>