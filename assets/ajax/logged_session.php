<?php
include_once('../config/connection.php');
session_start();
if (isset($_SESSION['uid'])) {
	// $json = $_SESSION['uid'];
	$loggedin = $_SESSION['uid'];
	$message = "success";

	$q = "SELECT * FROM users where email = '$loggedin'";
	$r = mysqli_query($dbc, $q);

	while ($row = mysqli_fetch_assoc($r)){

		$id = $row['id'];
		$type_id = $row['type_id'];
		$avatar = $row['avatar'];
		$name = $row['name'];
		$username = $row['username'];
		$email = $row['email'];
		$phone = $row['phone'];
		$next_kin = $row['next_kin'];
		$kin_phone = $row['kin_phone'];
		$dob = $row['dob'];
		$national_id = $row['national_id'];
		$nhif = $row['nhif'];
		$nssf = $row['nssf'];
		$kra_pin = $row['kra_pin'];
		$department = $row['department'];
		$bank_name = $row['bank_name'];
		$bank_branch = $row['bank_branch'];
		$acc_number = $row['acc_number'];
		$date_added = $row['date_added'];

		$json[] = array('message'=> $message, 'uid'=> $id, 'usertype'=> $type_id, 'avatar'=> $avatar, 'name'=> $name, 'username'=> $username, 'useremail'=> $email, 'userphone'=> $phone, 'kinname'=> $next_kin, 'kinphone'=> $kin_phone, 'dob'=> $dob, 'nationalid'=> $national_id, 'nhif'=> $nhif, 'nssf'=> $nssf, 'krapin'=> $kra_pin, 'department'=> $department, 'bankname'=> $bank_name, 'bankbranch'=> $bank_branch, 'accnumber'=> $acc_number, 'dateadded'=> $date_added);
	}
}
header('Content-type: application/json');
echo json_encode($json, JSON_PRETTY_PRINT);
?>