<?php ## CSS Include File ?>
<link href="/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="/assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<!-- <link href="/assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css"> -->
<link href="/assets/plugins/sweetalert/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="/assets/plugins/morris/morris.css">

<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/core.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/components.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/pages.css" rel="stylesheet" type="text/css" />
<link href="/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="/css/app.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/plugins/clock/style.css" />
<?php if ($page == 'login'): ?>
    <style>
    	.footer{
    		left: 0;
    	}
    </style>
<?php endif; ?>
<?php if ($page == 'day'): ?>
    <style>
    	.assignedname{
    		margin: 0 15% 0 0;
        	top: 0px;
    	}
    </style>
<?php endif; ?>
<?php if ($page == 'display'): ?>
   <style type="text/css">
		.topbar{
			display: none !important;
		}
		.side-menu{
			display:none !important;
		}
		.content-page{
			margin-left:0;
		}
		.footer{
			left: 0px;
		}
		.content-page > .content{
			margin-top:0;
		}
		.full{
			cursor:pointer;
		}
		.breadcrumb{
			padding:0 !important;
			margin-bottom:5px !important;
		}
		.breadcrumb li a:link{
			color: #797979 !important;
		}
		.breadcrumb li a:hover{
			color: #797979 !important;
		}
		.breadcrumb li a:visited{
			color: #797979 !important;
		}
		.breadcrumb li a:active{
			color: #797979 !important;
		}
		.actionarea{
			display:none !important;
		}
		.assignedname{
			font-size:2.5em;
			width: 450px;
			margin: -20px 0% 0 0;
		}
	</style>
<?php endif; ?>
