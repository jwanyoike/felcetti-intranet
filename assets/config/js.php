
<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<!-- <script src="/assets/js/jquery.min.js"></script> -->
<script type="text/javascript" src="/assets/Datepicker/jquery-2.1.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/detect.js"></script>
<script src="/assets/js/fastclick.js"></script>

<script src="/assets/js/jquery.slimscroll.js"></script>
<script src="/assets/js/jquery.blockUI.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/jquery.nicescroll.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>

<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>
        <!-- Sweet-Alert  -->


<script src="/assets/plugins/peity/jquery.peity.min.js"></script>



<!-- jQuery  -->
<script src="/assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="/assets/plugins/counterup/jquery.counterup.min.js"></script>

<script src="/assets/plugins/jquery-knob/jquery.knob.js"></script>

<script src="/scripts/angular.min.js" type="text/javascript"></script>
<script src="/scripts/angular-timeago.js" type="text/javascript"></script>
<script src="/scripts/angular-sanitize.js" type="text/javascript"></script>
<script src="/scripts/main.js" type="text/javascript"></script>
<!-- App -->
<script src="/scripts/callbacks-app.js" type="text/javascript"></script>
<script src="/scripts/dropzone.js" type="text/javascript"></script>
<!-- <script>
Dropzone.options.myDropzone = {
  init: function() {
      thisDropzone = this;
      // $.get('upload.php', function(data) {
      //     $.each(data, function(key,value){
      //         var mockFile = { name: value.name, size: value.size };
      //         thisDropzone.emit("addedfile", mockFile);
      //         thisDropzone.emit("thumbnail", mockFile, "uploads/"+value.name);
      //     });
      // });
      thisDropzone.on("addedfile", function(file) {
        var removeButton = Dropzone.createElement("<button>Remove</button>");
        var _this = this;
        removeButton.addEventListener("click", function(e) {
          e.preventDefault();
          e.stopPropagation();
          _this.removeFile(file);
        });
        file.previewElement.appendChild(removeButton);
        var record_id = $('.record_id').val();
        alert(record_id);
        alert(file.name);
      });

   thisDropzone.on("removedfile", function(file) {
    if (!file.serverId) { return; }
    $.post("delete-file.php?id=" + file.serverId);
  });

  }

};
</script> -->

<script type="text/javascript">
Dropzone.options.myAwesomeDropzone = {
  maxFilesize: 20, // Size in MB
  addRemoveLinks: true,
    removedfile: function(file) {
      var fileRef;
      return (fileRef = file.previewElement) != null ?
          fileRef.parentNode.removeChild(file.previewElement) : void 0;
    },
    success: function(file, response) {
      if (response.status == "success") {
        var new_image = response.message;
        var record_id = $('.record_id').val();
        $.post('/ajax/replaceimage.php', {new_image: new_image, record_id: record_id},function (data){
  				switch (data.status) {
  					case "error":
  						swal('Error', data.message, 'error');
  					break;
  					case "success":
  						swal('Success', data.message, 'success' );
  					break;
  				}
  			});
      }else {
        swal('Error', response.message, 'error');
      }
    },
    error: function(file, response) {
      alert(response.message);
    }
};
</script>
<!-- Filters -->
<!-- <script src="/scripts/filters/numberFilter.js" type="text/javascript"></script> -->
<script src="/scripts/filters/angular-timestamp-filter.min.js"></script>
<!-- Controllers -->
<script src="/scripts/controllers/leadsController.js"></script>
<script src="/scripts/controllers/usersController.js"></script>
<!-- Directives -->
<script src="/scripts/directives/pagination.js"></script>
<!-- Services -->
<script src="/scripts/services/dataService.js"></script>

<script src="/scripts/ajax.js" type="text/javascript"></script>
<script src="/scripts/jquery.form.js" type="text/javascript"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="/assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/js/modernizr.min.js"></script>

<!-- <script src="/assets/plugins/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="/assets/plugins/sweetalert/dist/sweetalert2.min.js"></script>

<!-- <script src="/assets/pages/jquery.sweet-alert.init.js"></script> -->
<script src="/assets/js/global.js"></script>
<script src="/assets/js/core-home.js"></script>


<!-- Modal-Effect -->

<script language="JavaScript">
//function to add zero infont of nymbers
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function calcTime(city, offset) {
    // create Date object for current location
    d = new Date();
    // convert to msec
    // add local time zone offset
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
    // return time as a string
    var h = addZero(nd.getHours());
    var m = addZero(nd.getMinutes());
    var s = addZero(nd.getSeconds());

    return h + ":" + m + "<span class='seconds'>." + s + "</span>";
}


$(document).ready(function() {
    bothtimes();
    window.setInterval(function(){
        bothtimes();
    }, 1000);
    function bothtimes(){
        $(".uktime").html(calcTime('London', '+1'));
        //kneya time
        $(".Nairobitime").html(calcTime('Kenya', '+3'));
    }

});
</script>

<script type="text/javascript">
    function notifyMe(over_due) {
        var counrnum = parseInt(over_due);
        //check if the browser supports notifications
        if (!("Notification" in window)) {
          alert("This browser does not support desktop notification");
        }

        //check if the user is okay to get some notification
        else if (Notification.permission === "granted") {
            // If it's okay create a notification
              var notification = new Notification('A callback is due to be called.', {
                icon: '/images/logo.png',
                body: over_due + " callback is due!"
              });
            // notification.onclick = function () {
            //     window.open("http://callbacks/display");
            // };
        }
        // Note, Chrome does not implement the permission static property
        // So we have to check for NOT 'denied' instead of 'default'
        else if (Notification.permission !== 'denied') {
          Notification.requestPermission(function (permission) {

            // Whatever the user answers, we make sure we store the information
            if(!('permission' in Notification)) {
              Notification.permission = permission;
            }

            //create a notification
            if (permission === "granted") {
              // define the notification
                  var notification = new Notification('A callback is due to be called.', {
                    icon: '/images/logo.png',
                    body: over_due + " callback is due!"
                  });
                // notification.onclick = function () {
                //     window.open("http://callbacks/display");
                // };
            }
          });
        }
    }
</script>
<?php if ($page != 'login'): ?>
  <script type="text/javascript">
    var overdue = setInterval( function(){
                        callback_count();
                    }, 300000);
    var overdue = setInterval( function(){
                        sesion_timer();
                    }, 10000);
  </script>
<?php endif; ?>
<?php if ($page == 'weekview'): ?>
    <script type="text/javascript">
    var week_interval = setInterval( function(){
                          week_data();
                      }, 30000);
    </script>
<?php endif; ?>
<?php if ($page == 'day'): ?>
    <script type="text/javascript">
        var day_interval = setInterval(function(){
                          day_data();
                      }, 5000);
    </script>
<?php endif; ?>
<?php if ($page == 'display'): ?>
    <script type="text/javascript">
        $(document).on('click', ".full" , function() {
            $("#btn-fullscreen").trigger("click");
        });
        var day_interval = setInterval(function(){
            day_data();
        }, 1000);
    </script>
<?php endif; ?>

<?php if ($page == 'overtime'): ?>
<link href="/assets/Datepicker/prettify-1.0.css" rel="stylesheet">
<link href="/assets/Datepicker/bootstrap-datetimepicker.css" rel="stylesheet">
<script async="" src="/assets/Datepicker/analytics.js"></script>
<!-- <script type="text/javascript" src="/assets/Datepicker/jquery-2.1.1.min.js"></script> -->
<!-- <script type="text/javascript" src="/assets/Datepicker/bootstrap.min.js"></script> -->
<script src="/assets/Datepicker/moment-with-locales.js"></script>
<script src="/assets/Datepicker/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="chrome-extension://aadgmnobpdmgmigaicncghmmoeflnamj/ng-inspector.js"></script>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker({
            format: "YYYY-MM-DD HH:mm:00"
        });
        $('#datetimepicker5').datetimepicker({
            format: "YYYY-MM-DD HH:mm:00"
        });
    });
</script>

<?php endif; ?>


<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

        jQuery('#datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        jQuery('#datepicker-inline').datepicker();
        jQuery('#datepicker-multiple-date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            clearBtn: true,
            multidate: true,
            multidateSeparator: ","
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });

        //Clock Picker
        $('.clockpicker').clockpicker({
            donetext: 'Done'
        });

        $('#single-input').clockpicker({
            placement: 'bottom',
            align: 'left',
            autoclose: true,
            'default': 'now'
                });
                $('#check-minutes').click(function(e){
                    // Have to stop propagation here
                    e.stopPropagation();
                    $("#single-input").clockpicker('show')
                        .clockpicker('toggleView', 'minutes');
                });

    });
</script>
