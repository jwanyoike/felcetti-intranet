<html>
	<head>
     <style type="text/css">
        .modal-content{
            background-color: rgba(255, 255, 2555, 0.9) !important;
        }
        .text-error i{
            font-size: 78px;
            padding: 0px 10px;
        }
        .modal-dialog{
            width: 500px !important;
        }
        .form-horizontal .control-label{
            text-align: left;
        }
        .form-trasparent{
            color: #000;
        }
        /* body{
            color:#ffffff !important;
        }
 */    </style>   
    </head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
            <div class="col-md-12">
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px;"><img src="/assets/images/logo.png"></div>
                
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px; text-align: right;">
                    <h5 class="modal-title">
                        Felcetti LTD<br/>
                        Tel: 0930503495<br>
                        P.O Box 68376 - 00200<br/>
                        Nairobi
                    </h5>
                </div>
            </div>
		</div>
		<div class="modal-body">

			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">{{pay.sal_date | timestamp | date: 'MMM, yyyy'}} Payslip</div>
                </div>
				<div class="col-md-12">
                	<div class="form-group">
                        <label class="col-md-8 control-label text-info">Personal Details</label>
                        <div class="col-md-4 text-info"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label>
                        <div class="col-md-8">
                            <input type="text" style="background-color:transparent;" disabled class="form-trasparent col-md-12 text-right" value="{{pay.names}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Department</label>
                        <div class="col-md-8">
                            <input type="text" style="background-color:transparent;" disabled class="form-trasparent col-md-12 text-right" value="{{pay.department}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info">PAYMENTS</label>
                        <div class="col-md-4 text-info text-right">
                            AMOUNT(KSH)
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-md-8 control-label">BASIC SALARY</label>
                    	<div class="col-md-4">
                    		<input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.basic}}">
                		</div>
            		</div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">OTHER BENEFITS</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.other}}">
                        </div>
                    </div>
            		<div class="form-group">
            			<label class="col-md-8 control-label">GROSS SALARY</label>
                		<div class="col-md-4">
                    		<input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.gross}}">
                		</div>
            		</div>
            		<div class="form-group">
                        <label class="col-md-8 control-label text-info">DEDUCTIONS</label>
                		<div class="col-md-4"></div>
            		</div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">PAYE</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.paye}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">PERSONAL RELIEF</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.p_relief}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NET PAYE</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.net_paye}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NSSF</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.nssf}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NHIF</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.nhif}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">TOTAL DEDUCTIONS</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.total_deduct}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info"></label>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-success">NET PAY</label>
                        <div class="col-md-4">
                            <input type="text" disabled class="form-trasparent col-md-12 text-right" value="{{pay.net_pay}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info">PAYMENT BY:- BANK TRANSFER</label>
                        <div class="col-md-4" style="text-align: right;">
                            <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                            <button ng-click="close()" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                <!-- end of module -->
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
        <script type="text/javascript">
      //colorpicker start
                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker0').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                jQuery('#datepicker1').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                // Date Picker
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple-date').datepicker({
                    format: "mm/dd/yyyy",
                    clearBtn: true,
                    multidate: true,
                    multidateSeparator: ","
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true
                });
    </script>
	</body>
</html>