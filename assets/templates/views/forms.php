<!-- <div> -->
<div>
    <div ng-repeat="log in logged">
		<ul class="nav navbar-nav navbar-right pull-right" style=" position: absolute; top: 0px; right: 15px; z-index: 1000;">
			<li class="dropdown">
		        <div class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img ng-src="assets/images/users/{{log.avatar}}" class="img-circle"/> </div>
		        <ul class="dropdown-menu">
		            <li ng-click="profileView(log)"><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
		            <li ng-click="settings(log)"><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
		            <span ng-if="log.usertype == '1'">
	                    <li><a ng-href="#/users"><i class="icon-people"></i> All users</a></li>
	                </span>
		            <input type="hidden" class="uemail_log" value="{{log.useremail}}">
		            <li ng-click="logout()"><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
		        </ul>
		    </li>
		</ul>
	</div>    
    <div class="col-md-12" ba-panel="" ba-panel-title="Modals" ba-panel-class="with-scroll">
	<div class="panel panel-blur with-scroll animated zoomIn" zoom-in="" ba-panel-blur="" style="background-size: 1273px 716px; background-position: 0px -128px;">

		<div class="panel-heading clearfix">
			<h3 class="panel-title">{{mtstxt}}</h3>
		</div>
		<div class="panel-body">
			<div class="modal-buttons clearfix ng-scope">
			<!-- users serach area -->
				<div class="row">
                	<div class="col-sm-4">
                		<form role="form">
                            <div class="form-group contact-search m-b-30">
                            	<input type="text" id="search" class="form-trasparent non-editable" placeholder="Search..." ng-model="filterleave" >
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                	</div>
                	

                	<div class="col-sm-8" style="text-align: right;">
	                	<span ng-repeat="log in logged">
	                		<button ng-click="leav_apply(log)" type="button" class="btn btn-info waves-effect waves-light"><span class="btn-label"><i class="fa fa-exclamation"></i></span>Apply for Leave</button>
					    </span>
                	</div>
                </div>
                <!-- users loading area -->
                <div class="table-responsive">
                    <table class="table table-hover mails m-0 table table-actions-bar">
                    	<thead>
							<tr>
								<th>Date Applied</th>
								<th>Department</th>
								<th>Reliever name</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						
                        <tbody class="list">
                        	<tr ng-repeat="leave in leave | filter:filterleave" >

                                <td style="width: 20%;">
	                                {{leave.date_applied | timestamp | date: "dd MMM, yyyy"}}
                                </td>

                                <td style="width: 20%;">
                         			{{leave.department}}
                                </td>
                                
                                <td style="width: 25%;">
                                	{{leave.relievername}}
                                </td>
                                
                                <td style="width: 22%;">
                                    <span class="label" ng-class="{'label-info': leave.status == '0', 'label-danger': leave.status == '1', 'label-success': leave.status == '2'}">{{leave.status == '0' ? 'Pending' : leave.status == '1' ? 'Rejected' : 'Approved'}}</span>
                                </td>
                                <td style="width: 13%;">
	                                <button class="btn btn-purple editable-table-button btn-xs" ng-click="view_applied_leave(leave)">View</button>
	                                <span ng-show="leave.user_id === log.usertype" ng-repeat="log in logged">
	                                	<span ng-if="log.usertype == '1'">
				                			<button class="btn btn-info editable-table-button btn-xs" ng-click="respond_to_leave(leave)">Respond</button>
				                		</span>
	                                </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

			</div>
		</div>
	</div>
</div>

<!-- <center><button ng-click="view()" class="btn btn-warning">Popup With Close</button></center> -->

<!-- <center><button ng-click="open()" class="btn btn-warning">Popup With Close</button></center> -->
</div>