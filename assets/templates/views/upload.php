<?php
//PHP code to upload file to server directory
if (!empty($_FILES)) {
    $name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];

    $sizes = array(128 => 128);
    $valid_formats = array("jpeg", "jpg", "png", "gif", "bmp");

    if (strlen($name)) {
    	list($txt, $ext) = explode(".", $name);
    	if(in_array( strtolower($ext), $valid_formats)){
    		if($size<(1024*1024)){
    			$ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
    			foreach ($sizes as $width => $height) {
            $file_extn = strtolower(end(explode('.', $name)));
            $file_temp = $_FILES['file']['tmp_name'];
            list($w, $h) = getimagesize($file_temp);
            $ratio = max($width/$w, $height/$h);
            $h = ceil($height / $ratio);
          	$x = ($w - $width / $ratio) / 2;
          	$w = ceil($width / $ratio);

            $path = '../assets/images/users/';
          	$new_image_name = substr(md5(time()), 0, 10) . '.' . $file_extn;

            /* read binary data from image file */
          	$imgString = file_get_contents($file_temp);
          	/* create image from string */
          	$image = imagecreatefromstring($imgString);
          	$tmp = imagecreatetruecolor($width, $height);
          	imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
            imagejpeg($tmp, $path.$new_image_name, 100);
            $results = array("status"=>"success", "message"=> $new_image_name);
    			}
    		}else {
    			$results = array("status"=>"error", "message"=>"Image file size max 1 MB");
    		}
    	}else {
    		$results = array("status"=>"error", "message"=>"Invalid file format..");
    	}

    }else {
    		$results = array("status"=>"error", "message"=>"try again.");
    }
}
header('Content-type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);
?>
