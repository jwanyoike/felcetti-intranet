<style type="text/css">
  .topbar, .side-menu, .right-bar{
    display:none;
  }
  .footer{
    left: 0px;
  }
</style>
<div class="col-md-6 col-md-offset-2" ba-panel="" ba-panel-title="Modals" ba-panel-class="with-scroll">
  <div class="panel panel-blur with-scroll animated zoomIn" zoom-in="" ba-panel-blur="" style="background-size: 1273px 716px; background-position: 0px -128px;">
    <div class="panel-heading clearfix">
      <h3 class="panel-title">{{txt}}</h3>
    </div>
    <div class="panel-body">
      <div class="modal-buttons clearfix ng-scope">
      <!-- users serach area -->
        <div class="row">
          
            <form role="form" name="loginform" class="form-horizontal ng-pristine ng-valid ng-valid-email">
              <div class="col-md-12">
                <div class="form-group">
                      <label class="col-md-3 control-label" style="padding-top: 5px;">Email</label>
                      <div class="col-md-8">
                        <input type="email" required autocomplete="off" name="email" ng-model="user_email" class="form-trasparent col-md-12" value="" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                      <label class="col-md-3 control-label" style="padding-top: 5px;">Password</label>
                      <div class="col-md-8">
                        <input type="password" required autocomplete="off" name="password" ng-model="user_password" class="form-trasparent col-md-12" value="" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" style="padding-top: 5px;"></label>
                    <div class="col-md-3">
                        <button ng-click="login(user)" ng-disabled="loginform.$invalid" type="submit" class="btn btn-success waves-effect waves-light">Login</button>
                    </div>
                    <div class="col-md-5" style="text-align: right;">
                        Forgot password?
                    </div>
                </div>
              </div>
            </form>
        </div>
        <!-- users loading area -->
      </div>
    </div>
  </div>
</div>