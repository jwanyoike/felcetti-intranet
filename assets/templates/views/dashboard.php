<div>
<div ng-repeat="log in logged">
	<ul class="nav navbar-nav navbar-right pull-right" style=" position: absolute; top: 0px; right: 15px; z-index: 1000;">
		<li class="dropdown">
	        <div class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img ng-src="assets/images/users/{{log.avatar}}" class="img-circle"/> </div>
	        <ul class="dropdown-menu">
	            <li ng-click="profileView(log)"><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
	            <li ng-click="settings(log)"><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
	            <span ng-if="log.usertype == '1'">
                    <li><a ng-href="#/users"><i class="icon-people"></i> All users</a></li>
                </span>
	            <input type="hidden" class="uemail_log" value="{{log.useremail}}">
	            <li ng-click="logout()"><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
	        </ul>
	    </li>
	</ul>
</div>
    <center><h3 style=" color: #ffffff;">{{txt}}</h3></center>
    <!-- <center><button ng-click="open()" class="btn btn-warning">Popup With Close</button></center> -->
</div>