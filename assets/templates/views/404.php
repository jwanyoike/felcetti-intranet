<!-- <div> -->
<div>
    <div class="col-md-8 col-md-offset-2" ba-panel="" ba-panel-title="Modals" ba-panel-class="with-scroll">
	<div class="panel panel-blur with-scroll animated zoomIn" zoom-in="" ba-panel-blur="" style="background-size: 1273px 716px; background-position: 0px -128px;">

		<div class="panel-heading clearfix">
			<span>
				<h3 class="panel-title">Attention !!</h3>
			</span>
		</div>
		<div class="panel-body">
			<div class="modal-buttons clearfix ng-scope">
			<!-- users serach area -->
				<div class="row">
                	<div class="col-md-12 text-center">
                		Page does not exist, Go back to <a ng-href="#/home"> home page</a>
                	</div>
                </div>
			</div>
		</div>
	</div>
</div>
</div>