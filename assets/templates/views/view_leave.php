<html>
	<head>
     <style type="text/css">
        .modal-content{
            background-color: rgba(255, 255, 255, 0.9) !important;
            /* background: url('/assets/images/agsquare.png'); */
        }
        .text-error i{
            font-size: 78px;
            padding: 0px 10px;
        }
        /* body{
            color:#ffffff !important;
        }
 */    </style>   
    </head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
            <div class="col-md-12">
                <div class="col-md-4" style="margin-top: -9px; margin-bottom: 6px;"><img src="/assets/images/logo.png"></div>
                <div class="col-md-4" style="margin-top: -9px; margin-bottom: 6px; text-align: center;">
                    <div class="text-error">
                        <i ng-class="{'ti-help-alt text-info': leave.status == '0', 'ti-face-sad text-danger': leave.status == '1', 'ti-face-smile text-success': leave.status == '2'}"></i><br/>
                        {{leave.status == '0' ? 'Pending' : leave.status == '1' ? 'Rejected' : 'Approved'}}
                    </div>
                    <!-- {{leave.status}} -->
                </div>
                <div class="col-md-4" style="margin-top: -9px; margin-bottom: 6px; text-align: right;">
                    <h5 class="modal-title">
                        Felcetti LTD<br/>
                        Tel: 0930503495<br>
                        P.O Box 68376 - 00200<br/>
                        Nairobi
                    </h5>
                </div>
            </div>
		</div>
		<div class="modal-body">

			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">LEAVE APPLICATION FORM (To be filled one month in advance)</div>
                </div>
				<div class="col-md-6">
                	<div class="form-group">
                        <label class="col-md-3 control-label">Applied on</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12" value="{{leave.date_applied | timestamp | date: 'dd MMM, yyyy'}}">
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-md-3 control-label">Name</label>
                    	<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12" value="{{leave.name}}">
                		</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Department</label>
                		<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12" value="{{leave.department}}">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">ID</label>
                		<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12" value="{{leave.user_id}}">
                		</div>
            		</div>
                    <div class="form-group"><label class="col-md-3 control-label">Tel</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 phonenumber" value="{{leave.phonenumber}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label">Email</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 usermail" value="{{leave.usermail}}">
                        </div>
                    </div>
                </div>
                
                <!-- end of first module -->
            	<div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Kin</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 kin" value="{{leave.kin}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label">Kin number</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 kinphone" value="{{leave.kinphone}}">
                        </div>
                    </div>
            		<div class="form-group"><label class="col-md-3 control-label">No of days</label>
                		<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12 nodays" value="{{leave.nodays}}">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">From</label>
                		<div class="col-md-8">
                    		<input type="text" id="datepicker0" disabled class="form-trasparent col-md-12 datefrom" value="{{leave.datefrom}}">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">To</label>
                		<div class="col-md-8">
                    		<input type="text" id="datepicker1" disabled class="form-trasparent col-md-12 dateto" value="{{leave.dateto}}">
                		</div>
            		</div>
            	</div>
                <div class="col-md-12" style="border-top: 1px solid #e5e5e5; padding-top: 20px; margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">Person to relieve the applicant while on annual Leave.</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Name</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 relievername" value="{{leave.relievername}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label">Department</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 relieverdepart" value="{{leave.relieverdepart}}">
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Comment</label>
                        <div class="col-md-8">
                            <textarea disabled class="form-trasparent col-md-12" style="resize: none;" rows="4">{{leave.comment}}</textarea>
                        </div>
                    </div>
                    <button style="position: absolute; right: -10px; bottom: -15px;" ng-click="close()" type="button" class="btn label label-danger waves-effect waves-light">Close</button>
                </div>

	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
        <script type="text/javascript">
      //colorpicker start
                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker0').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                jQuery('#datepicker1').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                // Date Picker
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple-date').datepicker({
                    format: "mm/dd/yyyy",
                    clearBtn: true,
                    multidate: true,
                    multidateSeparator: ","
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true
                });
    </script>
	</body>
</html>