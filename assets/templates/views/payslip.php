<!-- <div> -->
<div>
<div ng-repeat="log in logged">
	<ul class="nav navbar-nav navbar-right pull-right" style=" position: absolute; top: 0px; right: 15px; z-index: 1000;">
		<li class="dropdown">
	        <div class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img ng-src="assets/images/users/{{log.avatar}}" class="img-circle"/> </div>
	        <ul class="dropdown-menu">
	            <li ng-click="profileView(log)"><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
	            <li ng-click="settings(log)"><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
	            <span ng-if="log.usertype == '1'">
                    <li><a ng-href="#/users"><i class="icon-people"></i> All users</a></li>
                </span>
	            <input type="hidden" class="uemail_log" value="{{log.useremail}}">
	            <li ng-click="logout()"><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
	        </ul>
	    </li>
	</ul>
</div>
    <div class="col-md-8 col-md-offset-2" ba-panel="" ba-panel-title="Modals" ba-panel-class="with-scroll">
	<div class="panel panel-blur with-scroll animated zoomIn" zoom-in="" ba-panel-blur="" style="background-size: 1273px 716px; background-position: 0px -128px;">

		<div class="panel-heading clearfix">
			<span ng-repeat="log in logged">
				<h3 class="panel-title">{{log.name}}'s {{mtstxt}}</h3>
			</span>
		</div>
		<div class="panel-body">
			<div class="modal-buttons clearfix ng-scope">
			<!-- users serach area -->
				<div class="row">
                	<div class="col-sm-4">
                		<form role="form">
                            <div class="form-group contact-search m-b-30">
                            	<input type="text" id="search" class="form-trasparent non-editable" placeholder="Search..." ng-model="filterleave" >
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                	</div>
                	<div class="col-sm-8 text-right">
                		<div ng-repeat="log in logged">
	                		<span ng-if="log.usertype == '1'">
			                    <button ng-click="addSlip()" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">Add slip</button>
			                </span>
		                </div>
                	</div>
                </div>
                <!-- users loading area -->
                <div class="table-responsive">
                    <table class="table table-hover mails m-0 table table-actions-bar">
                    	<thead>
							<tr>
								<th>Month's pay</th>
								<th>Date Posted</th>
								<th>Department</th>
							</tr>
						</thead>
						
                        <tbody class="list">
                        	<tr ng-repeat="pay in pays | filter:filterleave" >
                        		<td style="width: 30%;">
	                                {{pay.sal_date | timestamp | date: "MMM, yyyy"}}
                                </td>
                                <td style="width: 30%;">
	                                {{pay.date_added | timestamp | date: "dd MMM, yyyy"}}
                                </td>
                                <td style="width: 35%;">
                         			{{pay.department}}
                                </td>
                                <td style="width: 5%;">
	                                <button class="btn btn-purple editable-table-button btn-xs" ng-click="view_payslip(pay)">View</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

			</div>
		</div>
	</div>
</div>

<!-- <center><button ng-click="view()" class="btn btn-warning">Popup With Close</button></center> -->

<!-- <center><button ng-click="open()" class="btn btn-warning">Popup With Close</button></center> -->
</div>