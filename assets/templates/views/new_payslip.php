<html>
	<head>
     <style type="text/css">
        .modal-content{
            background-color: rgba(255, 255, 2555, 0.9) !important;
        }
        .text-error i{
            font-size: 78px;
            padding: 0px 10px;
        }
        .modal-dialog{
            width: 500px !important;
        }
        .form-horizontal .control-label{
            text-align: left;
        }
        .form-trasparent{
            color: #000;
        }
        /* body{
            color:#ffffff !important;
        }
 */    </style>   
    </head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
            <div class="col-md-12">
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px;"><img src="/assets/images/logo.png"></div>
                
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px; text-align: right;">
                    <h5 class="modal-title">
                        Felcetti LTD<br/>
                        Tel: 0930503495<br>
                        P.O Box 68376 - 00200<br/>
                        Nairobi
                    </h5>
                </div>
            </div>
		</div>
		<div class="modal-body">

			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">{{sal_month}} Payslip</div>
                </div>
				<div class="col-md-12">
                	<div class="form-group">
                        <label class="col-md-8 control-label text-info">Personal Details</label>
                        <div class="col-md-4 text-info"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label>
                        <div class="col-md-6" style="float: right;">
                            <select class="form-control col-md-12 names text-right emp_name" style="background-color: transparent;" ng-model="user.id" ng-options="user.id as user.name for user in users">
                                <option value="">Employee name</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Department</label>
                        <div class="col-md-6" style="float: right;">
                            <select disabled class="form-control col-md-12 names text-right emp_depart" style="background-color: transparent;" ng-model="user.id" ng-options="user.id as user.department for user in users" ng-change="updateObjects(user.id)">
                                <option value=""></option>
                            </select>
                            <input type="text" style="display: none;" class="emp_id" ng-model="user.id" ng-options="user.id for user in users">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Email</label>
                        <div class="col-md-6" style="float: right;">
                            <select disabled class="form-control col-md-12 names text-right emp_email" style="background-color: transparent;" ng-model="user.id" ng-options="user.id as user.email for user in users" ng-change="updateObjects(user.id)">
                                <option value="" selected></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date</label>
                        <div class="col-md-6 text-right" style="float: right;">
                            <input type="text" ng-model="sal_month" class="form-trasparent" placeholder="salary month" id="datepicker-autoclose">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info">PAYMENTS</label>
                        <div class="col-md-4 text-info text-right">
                            AMOUNT(KSH)
                        </div>
                    </div>
                    <div class="form-group">
                    	<label class="col-md-8 control-label">BASIC SALARY</label>
                    	<div class="col-md-4">
                    		<input type="number" ng-model="basic" class="form-trasparent col-md-12 text-right" value="">
                		</div>
            		</div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">OTHER BENEFITS</label>
                        <div class="col-md-4">
                            <input type="number" ng-model="other_ben" class="form-trasparent col-md-12 text-right" value="">
                        </div>
                    </div>
            		<div class="form-group">
            			<label class="col-md-8 control-label">GROSS SALARY</label>
                		<div class="col-md-4">
                            <input type="text" class="form-trasparent col-md-12 text-right gross_sal" value="{{basic + other_ben}}">
                		</div>
            		</div>
            		<div class="form-group">
                        <label class="col-md-8 control-label text-info">DEDUCTIONS</label>
                		<div class="col-md-4"></div>
            		</div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">PAYE</label>
                        <div class="col-md-4">
                            <input type="number" ng-model="paye" class="form-trasparent col-md-12 text-right" value="{{pay.paye}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">PERSONAL RELIEF</label>
                        <div class="col-md-4">
                            <input type="number" ng-model="p_relief" class="form-trasparent col-md-12 text-right" value="{{pay.p_relief}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NET PAYE</label>
                        <div class="col-md-4">
                            <input type="number" class="form-trasparent col-md-12 text-right paye_bal" value="{{paye - p_relief}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NSSF</label>
                        <div class="col-md-4">
                            <input type="number" ng-model="nssf" class="form-trasparent col-md-12 text-right" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">NHIF</label>
                        <div class="col-md-4">
                            <input type="number" ng-model="nhif" class="form-trasparent col-md-12 text-right" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label">TOTAL DEDUCTIONS</label>
                        <div class="col-md-4">
                            <input type="text" class="form-trasparent col-md-12 text-right total_deduct" value="{{(paye - p_relief) + nssf + nhif}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info"></label>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-success">NET PAY</label>
                        <div class="col-md-4">
                            <input type="text" class="form-trasparent col-md-12 text-right net_pay" value="{{(basic + other_ben) - ((paye - p_relief) + nssf + nhif)}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 control-label text-info">PAYMENT BY:- BANK TRANSFER</label>
                        <div class="col-md-4" style="text-align: right;">
                            <!-- <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a> -->
                            <button ng-click="Add_sal()" type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">Add</button>
                            <button ng-click="close()" type="button" class="btn btn-danger waves-effect waves-light" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                <!-- end of module -->
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
        <script type="text/javascript">
      //colorpicker start
                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker0').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                jQuery('#datepicker-autoclose').datepicker({
                    format: "mm-dd-yyyy",
                    autoclose: true,
                    todayHighlight: true
                });
                jQuery('#datepicker1').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                // Date Picker
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple-date').datepicker({
                    format: "mm/dd/yyyy",
                    clearBtn: true,
                    multidate: true,
                    multidateSeparator: ","
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true
                });
    </script>
	</body>
</html>