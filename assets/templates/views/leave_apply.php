<html>
	<head>
     <style type="text/css">
        .modal-content{
            background-color: rgba(104, 233, 210, 0.86) !important;
        }
        /* body{
            color:#ffffff !important;
        }
 */    </style>   
    </head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
            <div class="col-md-12">
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px;"><img src="/assets/images/logo.png"></div>
                <div class="col-md-6" style="margin-top: -9px; margin-bottom: 6px; text-align: right;">
                    <h5 class="modal-title">
                        Felcetti LTD<br/>
                        Tel: 0930503495<br>
                        P.O Box 68376 - 00200<br/>
                        Nairobi
                    </h5>
                </div>
            </div>
		</div>
		<div class="modal-body">

			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">LEAVE APPLICATION FORM (To be filled one month in advance)</div>
                </div>
				<div class="col-md-6">
                	<div class="form-group">
                    	<label class="col-md-3 control-label">Name</label>
                    	<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12 names" value="{{log.name}}">
                		</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">Department</label>
                		<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12 department" value="{{log.department}}">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">ID</label>
                		<div class="col-md-8">
                    		<input type="text" disabled class="form-trasparent col-md-12 uid" value="{{log.uid}}">
                		</div>
            		</div>
                    <div class="form-group"><label class="col-md-3 control-label">Tel</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 phonenumber" value="{{log.userphone}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label">Email</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 usermail" value="{{log.useremail}}">
                        </div>
                    </div>
                </div>
                
                <!-- end of first module -->
            	<div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Kin</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 kin" value="{{log.kinname}}">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label">Kin number</label>
                        <div class="col-md-8">
                            <input type="text" disabled class="form-trasparent col-md-12 kinphone" value="{{log.kinphone}}">
                        </div>
                    </div>
            		<div class="form-group"><label class="col-md-3 control-label">No of days</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" class="form-trasparent col-md-12 nodays" value="">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">From</label>
                		<div class="col-md-8">
                    		<input type="text" id="datepicker0" autocomplete="off" class="form-trasparent col-md-12 datefrom" value="">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">To</label>
                		<div class="col-md-8">
                    		<input type="text" id="datepicker1" autocomplete="off" class="form-trasparent col-md-12 dateto" value="">
                		</div>
            		</div>
            	</div>
                <div class="col-md-12" style="border-top: 1px solid #e5e5e5; padding-top: 20px; margin-bottom: 10px;">
                    <div class="form-group header-title" style="letter-spacing: 1px;">Person to relieve the applicant while on annual Leave.</div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Name</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" class="form-trasparent col-md-12 relievername" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"><label class="col-md-3 control-label">Department</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" class="form-trasparent col-md-12 relieverdepart" value="">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-md-3 control-label"></label>
                        <div class="col-md-5">
                            <button ng-click="postLeaveData()" type="submit" class="btn btn-success waves-effect waves-light">Apply</button>
                        </div>
                        <div class="col-md-3">
                            <button ng-click="close()" type="button" class="btn btn-danger waves-effect waves-light closeleave" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>

	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
        <script type="text/javascript">
      //colorpicker start
                // Date Picker
                jQuery('#datepicker').datepicker();
                jQuery('#datepicker0').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                jQuery('#datepicker1').datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: "mm-dd-yyyy"
                });
                // Date Picker
                jQuery('#datepicker-inline').datepicker();
                jQuery('#datepicker-multiple-date').datepicker({
                    format: "mm/dd/yyyy",
                    clearBtn: true,
                    multidate: true,
                    multidateSeparator: ","
                });
                jQuery('#date-range').datepicker({
                    toggleActive: true
                });
    </script>
	</body>
</html>