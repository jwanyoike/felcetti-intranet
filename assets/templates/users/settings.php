<html>
	<head>
		<style>
			.modal-content{
	            background-color: rgba(255, 255, 2555, 0.9) !important;
	        }
	        .text-error i{
	            font-size: 78px;
	            padding: 0px 10px;
	        }
	        .modal-dialog{
	            width: 500px !important;
	        }
	        .form-horizontal .control-label{
	            text-align: left;
	        }
	        .form-trasparent{
	            color: #000;
	        }
	        .preview
			{
				width:200px;
				border:solid 1px #dedede;
				padding:10px;
			}
			#preview
			{
				color:#cc0000;
				font-size:12px;
			}
		</style>
		<script type="text/javascript" >
	      $(document).ready(function() {
	        	$("#imageform").on('submit',(function(e) {
				    e.preventDefault();
				    // $("#message").empty();
				    // $('#loading').show();
				    var id = $('.user_id').val();
				    if (id == "") {
				        swal('Error', 'Please try again', 'error');
				      }else {
				        $.ajax({
					        url: "../assets/ajax/ajaximage.php", // Url to which the request is send
					        type: "POST",             // Type of request to be send, called as method
					        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					        contentType: false,      // The content type used when sending data to the server.
					        cache: false,             // To unable request pages to be cached
					        processData:false,        // To send DOMDocument or non processed data file it is set to false
					        success: function(datas)   // A function to be called if request succeeds
						        {
						            if(datas.status == "success"){
						            	$("#preview").html(datas.message);
						                // swal('Added!', datas.message, 'success');
						            }else{
						                swal('Error', datas.message, 'error');
						            }
						        }
				        });
				      }
				  }));
	        });

	    </script>
		
	</head>
	<body>

	<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
			<h4 class="modal-title">{{log.name}}'s profile image</h4>
		</div>
		<div class="modal-body">
			<form id="imageform" class="form-horizontal" method="post" action='../assets/ajax/ajaximage.php' enctype="multipart/form-data">
			<!-- Upload your image<input type="file" name="photoimg" id="photoimg" /> -->
			<input type="hidden" value="{{log.uid}}" name="user_id" readonly class="form-control user_id">
			<input type="hidden" value="{{log.avatar}}" name="avatar" readonly class="form-control avatar">
				<div class="col-md-12">
	            	<div class="form-group">
	                	<label class="col-md-3 control-label"></label>
	                	<div class="col-md-8" id="preview">
	                		<img ng-src="assets/images/users/{{log.avatar}}" class="img-responsive img-rounded"/>
	            		</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="col-md-3 control-label"></label>
	                	<div class="col-md-8" id="preview">
	                		<input type="file" name="photoimg" id="photoimg" class="filestyle" data-icon="false" data-buttonname="btn-white">
	            		</div>
					</div>
	                
	            	<div class="form-group">
	                	<label class="col-md-3 control-label">Name</label>
	                	<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{log.name}}">
	                		
	            		</div>
	        		</div>
	            </div>
	            <!-- end of first module -->
	        	<div class="col-md-12">
	        		<div class="form-group"><label class="col-md-3 control-label"></label>
	            		<div class="col-md-4">
	                		<button type="submit" class="btn btn-success waves-effect waves-light">Change</button>
	            		</div>
	            		<div class="col-md-4" style="text-align: right;">
	                		<button ng-click="close()" type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Close</button>
	            		</div>
	        		</div>
	        	</div>
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>

		<script type="text/javascript">
			$(":file").filestyle({input: false});
		</script>

	</body>
</html>