<html>
	<head></head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
			<h4 class="modal-title">{{user.name}} 's details</h4>
		</div>
		<div class="modal-body">
			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="col-md-6">
	            	<div class="form-group">
	                	<label class="col-md-3 control-label"></label>
	                	<div class="col-md-8">
	                		<img ng-src="assets/images/users/{{user.avatar}}" class="img-responsive img-rounded"/>
	            		</div>
	        		</div>
	                
	            	<div class="form-group">
	                	<label class="col-md-3 control-label">Name</label>
	                	<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.name}}">
	            		</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="col-md-3 control-label">D.O.B</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.dob}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">National ID</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.national_id}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Email</label>
	            		<div class="col-md-8">
	            			<input type="text" disabled class="form-control" value="{{user.email}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Phone</label>
	            		<div class="col-md-8">
	            			<input type="text" disabled class="form-control" value="{{user.phone}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Kin</label>
	            		<div class="col-md-8">
	            			<input type="text" disabled class="form-control" value="{{user.next_kin}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Kin phone</label>
	            		<div class="col-md-8">
	            			<input type="text" disabled class="form-control" value="{{user.kin_phone}}">
	            		</div>
	        		</div>
	            </div>
	            <!-- end of first module -->
	        	<div class="col-md-6">
	        		<div class="form-group"><label class="col-md-3 control-label">NHIF</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.nhif}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">NSSF</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.nssf}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">KRA PIN</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.kra_pin}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Department</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.department}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Bank name</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.bank_name}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Branch</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.bank_branch}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Acc No.</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.acc_number}}">
	            		</div>
	        		</div>

	        		<div class="form-group"><label class="col-md-3 control-label">Username</label>
	            		<div class="col-md-8">
	                		<input type="text" disabled class="form-control" value="{{user.username}}">
	            		</div>
	        		</div>

	        		<div class="form-group"><label class="col-md-3 control-label">Type Id</label>
	            		<div class="col-md-4">
	                		<input type="text" disabled class="form-control" value="{{user.type_id == '1' ? 'Admin' : user.type_id == '2' ? 'User' : 'Unknown'}}">
	            		</div>
	            		<div class="col-md-4" style="text-align: right;">
	                		<button ng-click="close()" type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Close</button>
	            		</div>
	        		</div>
	        	</div>
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
	</body>
</html>