<html>
	<head></head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
			<h4 class="modal-title">New user</h4>
		</div>
		<div class="modal-body">
			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="col-md-6">
                	<div class="form-group">
                    	<label class="col-md-3 control-label">Name</label>
                    	<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="names" ng-model="names" class="form-control names" value="" placeholder="Name">
                		</div>
            		</div>
            		<div class="form-group">
            			<label class="col-md-3 control-label">D.O.B</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="birth" ng-model="birth" class="form-control dob" value="" placeholder="Date of birth">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">ID</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="national" ng-model="national" class="form-control national" value="" placeholder="National ID">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Email</label>
                		<div class="col-md-8">
                			<input type="email" autocomplete="off" name="usremail" ng-model="usremail" class="form-control usremail" value="" placeholder="Email">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Phone</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="national" ng-model="pnone_number" class="form-control national" value="" placeholder="Phone number">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Kin</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="national" ng-model="next_ok" class="form-control national" value="" placeholder="Next of kin">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Kin phone</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="national" ng-model="next_ok_ph" class="form-control national" value="" placeholder="Next of kin phone">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">NHIF</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="nhif" ng-model="nhif" class="form-control nhif" value="" placeholder="nhif">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">NSSF</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="nssf" ng-model="nssf" class="form-control nssf" value="" placeholder="nssf">
                		</div>
            		</div>
                    <div class="form-group"><label class="col-md-3 control-label">Avatar</label>
                        <div class="col-md-8">
                            <input type="file" name="image" class="filestyle avatar" data-input="false" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                            <div class="bootstrap-filestyle input-group">
                                <span class="group-span-filestyle " tabindex="0">
                                    <label for="filestyle-1" class="btn btn-default ">
                                        <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                        <span class="buttonText">Choose file</span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of first module -->
            	<div class="col-md-6">
            		<div class="form-group"><label class="col-md-3 control-label">KRA PIN</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="krapin" ng-model="krapin" class="form-control krapin" value="" placeholder="kra pin">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Department</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="department" ng-model="department" class="form-control department" value="" placeholder="department">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Bank name</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="bankname" ng-model="bankname" class="form-control dankname" value="" placeholder="bank name">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Branch</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="branch" ng-model="branch" class="form-control branch" value="" placeholder="branch">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Acc No.</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="accnumber" ng-model="accnumber" class="form-control acc" value="" placeholder="acc number">
                		</div>
            		</div>

            		<div class="form-group"><label class="col-md-3 control-label">Username</label>
                		<div class="col-md-8">
                    		<input type="text" autocomplete="off" name="username" ng-model="username" class="form-control username" value="" placeholder="username">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Password</label>
                		<div class="col-md-8">
                    		<input type="password" autocomplete="off" name="password" ng-model="password" class="form-control password" value="" placeholder="password">
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Repeat Pass</label>
                		<div class="col-md-8">
                    		<input type="password" autocomplete="off" name="passwordagain" ng-model="passwordagain" class="form-control passwordagain" value="" placeholder="password again">
                		</div>
            		</div>

            		<div class="form-group"><label class="col-md-3 control-label">Type Id</label>
                		<div class="col-md-8">
                    		<select name="type_id" ng-model="type_id" class="form-control type_id">
	                    		<option value="">Type ID</option>
	                    		<option value="1">1</option>
	                    		<option value="2">2</option>
	                    		<option value="3">3</option>
                    		</select>
                		</div>
            		</div>
            		<div class="form-group"><label class="col-md-3 control-label">Avatar</label>
                		<div class="col-md-5">
                    		<button ng-click="addUser()" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                		</div>
                        <div class="col-md-3">
                            <button ng-click="close()" type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Cancel</button>
                        </div>
            		</div>
            	</div>
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
	</body>
</html>