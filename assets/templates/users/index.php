<div ng-controller="UsersShowController">

<div ng-repeat="log in logged">
    <ul class="nav navbar-nav navbar-right pull-right" style=" position: absolute; top: 0px; right: 15px; z-index: 1000;">
        <li class="dropdown">
            <div class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img ng-src="assets/images/users/{{log.avatar}}" class="img-circle"/> </div>
            <ul class="dropdown-menu">
                <li ng-click="profileView(log)"><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                <li ng-click="settings(log)"><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                <span ng-if="log.usertype == '1'">
                    <li><a ng-href="#/users"><i class="icon-people"></i> All users</a></li>
                </span>
                <input type="hidden" class="uemail_log" value="{{log.useremail}}">
                <li ng-click="logout()"><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>

    <div class="col-md-12" ba-panel="" ba-panel-title="Modals" ba-panel-class="with-scroll">
	<div class="panel panel-blur with-scroll animated zoomIn" zoom-in="" ba-panel-blur="" style="background-size: 1273px 716px; background-position: 0px -128px;">

		<div class="panel-heading clearfix">
			<h3 class="panel-title">Users</h3>
		</div>
		<div class="panel-body">
			<div class="modal-buttons clearfix ng-scope">
			<!-- users serach area -->
				<div class="row">
                	<div class="col-sm-4">
                		<form role="form">
                            <div class="form-group contact-search m-b-30">
                            	<input type="text" id="search" class="form-trasparent non-editable" placeholder="Search..." ng-model="filterusers" >
                                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                            </div> <!-- form-group -->
                        </form>
                	</div>
                	<div class="col-sm-8" style="text-align: right;">
                        <span ng-repeat="log in logged">
                            <span ng-if="log.usertype == '1'">
                                <button ng-click="addUser_module()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#panel-modal">Add user</button>
                            </span>
                        </span>
                	</div>
                </div>





                <!-- users loading area -->
                <div class="row">
                    <div class="col-sm-6 col-lg-4" ng-repeat="user in users | filter:filterusers" ng-class="{'editing-user': editing, 'edited': user.edited}">
                        <div class="card-box">
                            <div class="contact-card">
                                <div class="pull-left">
                                    <img ng-src="assets/images/users/{{user.avatar}}" class="img-circle thumb-sm" />
                                </div>
                                <div class="member-info">
                                    <h4 class="m-t-0 m-b-5 header-title"><b>{{user.name}}</b></h4>
                                    <p class="text-muted">{{user.department}}</p>
                                    <p class="text-dark"><i class="md md-business m-r-10"></i><small>{{user.email}}</small></p>
                                    <div class="contact-action">
                                        <ul class="social-links list-inline m-0">
                                            <li>
                                                <a ng-click="edit(user)" title="" data-placement="top" data-toggle="tooltip" class="tooltips btn-success" href="" data-original-title="Facebook"><i class="md md-mode-edit"></i></a>
                                            </li>
                                            <li>
                                                <a ng-click="view(user)" title="" data-placement="top" data-toggle="tooltip" class="tooltips btn-info" href="" data-original-title="Twitter"><i class="md md-done-all"></i></a>
                                            </li>
                                            <li>
                                                <a ng-click="removeUser(user, $index)" title="" data-placement="top" data-toggle="tooltip" class="tooltips btn-danger" href="" data-original-title="LinkedIn"><i class="md md-delete"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>







                <!-- <div class="table-responsive">
                    <table class="table table-hover mails m-0 table table-actions-bar">
                    	<thead>
							<tr>
								<th>Avatar</th>
								<th>Name</th>
								<th>Email</th>
								<th>Start Date</th>
								<th>Action</th>
							</tr>
						</thead>
						
                        <tbody class="list">
                        	<tr ng-repeat="user in users | filter:filterusers" ng-class="{'editing-user': editing, 'edited': user.edited}">

                                <td style="width: 10%;">
	                                <img ng-src="assets/images/users/{{user.avatar}}" class="img-circle thumb-sm" />
                                </td>
                                
                                <td style="width: 28%;">
                                	<span ng-hide="editing">{{user.name}}</span>
                                	<input ng-change="user.edited = true" ng-blur="editing = false;" ng-show="editing" class="form-trasparent editable" type="text" ng-model="user.name">
                                </td>
                                
                                <td style="width: 25%;">
                                	<span ng-hide="editing">{{user.email}}</span>
                                	<input ng-change="user.edited = true" ng-blur="editing = false;" ng-show="editing" class="form-trasparent editable" type="text" ng-model="user.email">
                                </td>
                                
                                <td style="width: 20%;">
                                    {{ user.date_added | timestamp | date: "dd MMM, yyyy" }}
                                </td>
                                <td style="width: 17%;">
	                                <button class="btn btn-info editable-table-button btn-xs" ng-click="view(user)">View</button>
                                	<button class="btn btn-primary editable-table-button btn-xs" ng-click="edit(user)">Edit</button>
                                	<button class="btn btn-success editable-table-button btn-xs" ng-show="editing" ng-click="saveUser(user)">save</button>
                                	<button class="btn btn-white btn-custom waves-effect btn-xs" ng-show="editing" ng-click="editing = !editing">Cancel</button>
                                	<button class="btn btn-danger editable-table-button btn-xs" ng-hide="editing" ng-click="removeUser(user, $index)">Delete</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> -->

			</div>
		</div>
	</div>
</div>
    <!-- <center><button ng-click="view()" class="btn btn-warning">Popup With Close</button></center> -->
</div>