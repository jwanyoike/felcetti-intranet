<html>
	<head>
		<style type="text/css">
			.modal-content{
				background-color: rgba(226, 242, 255, 0.93) !important;
			}
		</style>
	</head>
	<body>
		<div class="modal-header" style="border-bottom-width: 1px; padding-bottom: 0px;">
			<h4 class="modal-title">Edit {{user.name}} 's details</h4>
		</div>
		<div class="modal-body">
			<form id="imageform" class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="col-md-6">
	            	<div class="form-group">
	                	<label class="col-md-3 control-label"></label>
	                	<div class="col-md-8">
	                		<input type="hidden" value="{{user.id}}" class="userid">
	                		<img ng-src="assets/images/users/{{user.avatar}}" class="img-responsive img-rounded"/>
	            		</div>
	        		</div>
	                
	            	<div class="form-group">
	                	<label class="col-md-3 control-label">Name</label>
	                	<div class="col-md-8">
	                		<input type="text" class="form-control name" value="{{user.name}}">
	            		</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="col-md-3 control-label">D.O.B</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control dob" value="{{user.dob}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">ID</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control national" value="{{user.national_id}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Email</label>
	            		<div class="col-md-8">
	            			<input type="text" class="form-control emailadd" value="{{user.email}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Phone</label>
	            		<div class="col-md-8">
	            			<input type="text" class="form-control phonenum" value="{{user.phone}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Kin</label>
	            		<div class="col-md-8">
	            			<input type="text" class="form-control kinname" value="{{user.next_kin}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Kin phone</label>
	            		<div class="col-md-8">
	            			<input type="text" class="form-control kinphone" value="{{user.kin_phone}}">
	            		</div>
	        		</div>
	            </div>
	            <!-- end of first module -->
	        	<div class="col-md-6">
	        		<div class="form-group"><label class="col-md-3 control-label">NHIF</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control nhifs" value="{{user.nhif}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">NSSF</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control nssfs" value="{{user.nssf}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">KRA PIN</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control krapin" value="{{user.kra_pin}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Department</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control departments" value="{{user.department}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Bank name</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control banknames" value="{{user.bank_name}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Branch</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control branchs" value="{{user.bank_branch}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Acc No.</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control numbers" value="{{user.acc_number}}">
	            		</div>
	        		</div>

	        		<div class="form-group"><label class="col-md-3 control-label">Username</label>
	            		<div class="col-md-8">
	                		<input type="text" class="form-control usernames" value="{{user.username}}">
	            		</div>
	        		</div>
	        		<div class="form-group"><label class="col-md-3 control-label">Type Id</label>
	            		<div class="col-md-3">
	            			<select class="form-control col-md-12 leave_reply" style="background-color: transparent;">
                                <option value="">{{user.type_id == '1' ? 'Admin' : user.type_id == '2' ? 'User' : 'Unknown'}}</option>
                                <option value="1">Admin</option>
                                <option value="2">User</option>
                            </select>

	                		<!-- <input type="text" class="form-control typeids" value="{{user.type_id}}"> -->
	            		</div>
	            		<div class="col-md-5" style="text-align: right;">
	            			<button ng-click="updateUser()" type="button" class="btn btn-success waves-effect waves-light">Update</button>
	                		<button ng-click="close()" type="button" class="btn btn-info waves-effect waves-light clossing">Close</button>
	            		</div>
	        		</div>
	        	</div>
	        </form>
		</div>
		<div class="modal-footer" style="border-top: none;">
			<!-- <button class="btn btn-warning" type="button" ng-click="close()">Close</button> -->
		</div>
	</body>
</html>