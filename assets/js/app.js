angular.module('Felcettiapp', ['srph.timestamp-filter','ui.bootstrap','ngRoute', 'ngResource','ngAnimate']);

angular.module('Felcettiapp').run(['dataService','$rootScope','$location','loginService', function(dataService,$rootScope,$location,loginService){
	var routespermision = ['/home','/users','/forms','/payslip'];
	$rootScope.$on('$routeChangeStart', function(){
		if ( routespermision.indexOf($location.path()) !=-1 ) {
			var connected = loginService.isLoggedin();
			connected.then(function(msg){
				if (!msg.data) {
					$location.path('/login');
				}
			});
		}
	});
}]);