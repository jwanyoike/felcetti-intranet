angular.module('Felcettiapp').config(function($routeProvider){
  $routeProvider
    .when('/login', {
      templateUrl: "assets/templates/views/login.php",
      controller: "loginCtrl"
    })
    .when('/home', {
      templateUrl: "assets/templates/views/dashboard.php",
      controller: "homeCtrl"
    })
    .when('/users', {
      templateUrl: "assets/templates/users/index.php",
      controller: "UsersShowController"
    })
    .when('/forms', {
      templateUrl: "assets/templates/views/forms.php",
      controller: "formCtrl"
    })
    .when('/payslip', {
      templateUrl: "assets/templates/views/payslip.php",
      controller: "payslipCtrl"
    })
    .when('/404', {
      templateUrl: "assets/templates/views/404.php"
    })
    .otherwise({
      redirectTo: '/login'
    })
});