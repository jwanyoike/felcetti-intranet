angular.module('Felcettiapp').service('dataService', function($http,loginService){
    this.getUsers = function(callback){
      $http.get('../assets/ajax/users.php').then(callback);
    }
    this.getLoggedinUser = function(callback){
      $http.get('../assets/ajax/logged_session.php').then(callback);
    }
    this.getleave = function(callback){
      $http.get('../assets/ajax/user_leave.php').then(callback);
    }
    this.getpays = function(callback){
      $http.get('../assets/ajax/user_payslips.php').then(callback);
    }
    this.helloConsole = function(){
      console.log('This is a saved data');
    }
    this.removeUser = function(user){
        swal({ title: "Please wait!", showConfirmButton: false });
        $http.post("../assets/ajax/delete_user.php",{'id':user.id}).success(function(data){
            if(data.status == "success"){
              swal('Deleted!', data.message, 'success');
            }else{
                swal('Error', data.message, 'error');
            }
        });
    }
    this.saveUser = function(user){
		swal('Saved!', user.name + "'s records saved!!" , 'success');
		console.log('This user details are saved');
    }
});; 