'use strict';
angular.module('Felcettiapp').factory('sessionService', ['$http', function($http) {
    return {
        set:function(key, value) {
            return sessionStorage.setItem(key,value);
        },
        get:function(key) {
            return sessionStorage.getItem(key);
        },
        destroy:function(key) {
            $http.post('../assets/ajax/session_destroy.php'); //destroy sessionen
            return sessionStorage.removeItem(key);
        }
    };
}]);