'use strict';
angular.module('Felcettiapp').factory('loginService', ['$http','$rootScope','$location','sessionService', function($http,$rootScope,$location,sessionService) {
    return {
        login:function(data, scope) {
            $http.post("../assets/ajax/login.php",{'email':scope.user_email,'password':scope.user_password}).success(function(data){
                if(data.status == "success"){
                    $rootScope.user = data.user;
                    sessionService.set(data.user,data.key);
                    $location.path('/home');
                }else{
                    swal('Error', data.message, 'error');
                }
            });
        },
        logout:function(key){
            sessionService.destroy(key);
            $location.path('/login');
        },
        isLoggedin:function(){
            var $checkSessionServer = $http.post('../assets/ajax/session_check.php');
            return $checkSessionServer;
        },
        isAdmin:function(){
            var $checkAdmin = $http.post('../assets/ajax/user_page_check.php');
            return $checkAdmin;
        },
        loggedSession:function(callback){
            $http.get('../assets/ajax/logged_session.php').then(callback);
        }
    };
}]);