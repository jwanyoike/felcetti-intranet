angular.module('Felcettiapp').controller('UsersShowController', ['$interval','loginService','dataService','$scope', '$http', '$location', '$uibModal', function($interval, loginService, dataService, $scope, $http, $location, $uibModal){
    $scope.pageClass = 'page-users';


    var connected = loginService.isAdmin();
    connected.then(function(msg){
        if (!msg.data) {
            $location.path('/404');
        }
    });

    $scope.logout = function() {
        var uemail_log = $('.uemail_log').val();
        loginService.logout(uemail_log);
    };
	dataService.getUsers(function(response){
	    $scope.users = response.data;
	});
    $interval(function(){
        // call users interval
        dataService.getUsers(function(response){
            $scope.users = response.data;
        });
    }, 5000);
    dataService.getLoggedinUser(function(response){
        $scope.logged = response.data;
    });
    
    $scope.addUser_module = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/new_user.php',
            controller: 'UserdataSave',
            size: size,
            resolve: {
                user: function () {
                    return p;
                }
            }
        });
    };
    
    $scope.addUser = function(){
    	var name = {name:$scope.names};
    	var email = {name:$scope.usremail};
    	$scope.date_added = "06 Mar, 2016";
    	$scope.users.push({ name: $scope.names, email: $scope.usremail, date_added: $scope.date_added, username: $scope.username });
    };

    $scope.removeUser = function (user, index) {
        swal({
          title: 'Delete ' + user.name +' ?',
          showCancelButton: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'No',
          closeOnConfirm: false,
          allowOutsideClick: false
        }).then(function(isConfirm) {
          if (isConfirm) {
            swal.enableLoading();
            dataService.removeUser(user);
            $scope.users.splice(index, 1);
          }
        })
    };

    $scope.saveUser = function(user){
    	dataService.saveUser(user);
    };

    $scope.view = function (p,size) {
        var modalInstance = $uibModal.open({
          templateUrl: '../assets/templates/users/show.php',
          controller: 'viewCtrl',
          size: size,
          resolve: {
            user: function () {
              return p;
            }
          }
        });
    };

    $scope.edit = function (p,size) {
        var modalInstance = $uibModal.open({
          templateUrl: '../assets/templates/users/edit_user.php',
          controller: 'editCtrl',
          size: size,
          resolve: {
            user: function () {
              return p;
            }
          }
        });
    };
    $scope.profileView = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/personalDetails.php',
            controller: 'personalData',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.settings = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/settings.php',
            controller: 'settingCtrl',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
}]);

angular.module('Felcettiapp').controller('viewCtrl', ['$scope', '$uibModalInstance', 'user', 'dataService',function ($scope, $uibModalInstance, user, dataService) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.user = angular.copy(user);
}]);

angular.module('Felcettiapp').controller('editCtrl', ['$scope', '$http', '$uibModalInstance', 'user', 'dataService',function ($scope, $http, $uibModalInstance, user, dataService) {
    $scope.user = angular.copy(user);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.updateUser = function () {
        var userid = $('.userid').val();
        var name = $('.name').val();
        var dob = $('.dob').val();
        var national = $('.national').val();
        var emailadd = $('.emailadd').val();
        var phonenum = $('.phonenum').val();
        var kinname = $('.kinname').val();
        var kinphone = $('.kinphone').val();
        var nhifs = $('.nhifs').val();
        var nssfs = $('.nssfs').val();
        var krapin = $('.krapin').val();
        var departments = $('.departments').val();
        var banknames = $('.banknames').val();
        var branchs = $('.branchs').val();
        var numbers = $('.numbers').val();
        var usernames = $('.usernames').val();
        var typeids = $('.typeids').val();

        swal({ title: "Please wait!", showConfirmButton: false });
            $http.post("../assets/ajax/update_users.php",{'id':userid,'names':name,'birth':dob,'national':national,'usremail':emailadd,'pnone_number':phonenum,'next_ok':kinname,'next_ok_ph':kinphone,'nhif':nhifs,'nssf':nssfs,'krapin':krapin,'department':departments,'bankname':banknames,'branch':branchs,'accnumber':numbers,'username':usernames,'type_id':typeids}).success(function(data){
            if(data.status == "success"){
                swal('Updated!', data.message, 'success');
                // $('.clossing').trigger(click);
                // $scope.users.push({ name: $scope.names, email: $scope.usremail, date_added: $scope.date_added, username: $scope.username });
            }else{
              swal('Error', data.message, 'error');
            }
        });
    };
}]);

angular.module('Felcettiapp').controller('UserdataSave', ['$scope', '$http', '$uibModalInstance', 'user', 'dataService',function ($scope, $http, $uibModalInstance, user, dataService) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.addUser = function () {
        if ($scope.names == null || $scope.birth == null || $scope.national == null || $scope.usremail == null || $scope.pnone_number == null || $scope.next_ok == null || $scope.next_ok_ph == null || $scope.nhif == null || $scope.nssf == null || $scope.krapin == null || $scope.department == null || $scope.bankname == null || $scope.branch == null || $scope.accnumber == null || $scope.username == null || $scope.password == null || $scope.passwordagain == null || $scope.type_id == null) {
                swal('Error', 'Please fill all the details', 'error');
        }else {
            swal({ title: "Please wait!", showConfirmButton: false });
            $http.post("../assets/ajax/new_users.php",{'names':$scope.names,'birth':$scope.birth,'national':$scope.national,'usremail':$scope.usremail,'pnone_number':$scope.pnone_number,'next_ok':$scope.next_ok,'next_ok_ph':$scope.next_ok_ph,'nhif':$scope.nhif,'nssf':$scope.nssf,'krapin':$scope.krapin,'department':$scope.department,'bankname':$scope.bankname,'branch':$scope.branch,'accnumber':$scope.accnumber,'username':$scope.username,'password':$scope.password,'passwordagain':$scope.passwordagain,'type_id':$scope.type_id}).success(function(data){
                if(data.status == "success"){
                    swal('Saved!', data.message, 'success');
                    $scope.names = null;
                    $scope.birth = null;
                    $scope.national = null;
                    $scope.usremail = null;
                    $scope.pnone_number = null;
                    $scope.next_ok = null;
                    $scope.next_ok_ph = null;
                    $scope.nhif = null;
                    $scope.nssf = null;
                    $scope.krapin = null;
                    $scope.department = null;
                    $scope.bankname = null;
                    $scope.branch = null;
                    $scope.accnumber = null;
                    $scope.username = null;
                    $scope.password = null;
                    $scope.passwordagain = null;
                    $scope.type_id = null;
                }else{
                  swal('Error', data.message, 'error');
              }
            });
        }
    };
}]);



