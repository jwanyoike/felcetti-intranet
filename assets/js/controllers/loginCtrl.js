'use strict';
angular.module('Felcettiapp').controller('loginCtrl', ['$scope','loginService', function($scope,loginService){
	$scope.txt = "Sign in to Felcetti";
	$scope.msgtxt = '';
	$scope.pageClass = 'page-login';
	$scope.login = function (user) {
		loginService.login(user, $scope);
	}
}]);