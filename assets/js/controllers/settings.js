'use strict';
angular.module('Felcettiapp').controller('settingCtrl', ['$scope', '$http', '$uibModalInstance', 'log', 'dataService',function ($scope, $http, $uibModalInstance, log, dataService) {
    $scope.txt = "Change your profile image";
    $scope.log = angular.copy(log);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);