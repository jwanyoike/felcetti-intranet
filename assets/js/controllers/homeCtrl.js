'use strict';
angular.module('Felcettiapp').controller('homeCtrl', ['dataService','$scope','loginService','$uibModal', function(dataService,$scope,loginService,$uibModal){
    $scope.pageClass = 'page-home';
	$scope.txt = "Welcome to Felcetti Intranet";
	dataService.getLoggedinUser(function(response){
        $scope.logged = response.data;
    });
	$scope.logout = function() {
		var uemail_log = $('.uemail_log').val();
        loginService.logout(uemail_log);
	};
	$scope.profileView = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/personalDetails.php',
            controller: 'personalData',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.settings = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/settings.php',
            controller: 'settingCtrl',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
}]);