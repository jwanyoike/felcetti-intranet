angular.module('Felcettiapp').controller('IndexController', ['$scope','$uibModal',function ($scope, $uibModal) {
    $scope.open = function () {
        var modalInstance = $uibModal.open({
			controller: 'PopupCont',
            templateUrl: '../assets/templates/users/show.html',
        });
    }
}]);

angular.module('Felcettiapp').controller('PopupCont', ['$scope','$uibModalInstance',function ($scope, $uibModalInstance) {
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
