angular.module('Felcettiapp').controller('personalData', ['$scope', '$http', '$uibModalInstance', 'log', 'dataService',function ($scope, $http, $uibModalInstance, log, dataService) {
    $scope.log = angular.copy(log);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);