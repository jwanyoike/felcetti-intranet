'use strict';
angular.module('Felcettiapp').controller('formCtrl', ['dataService','$interval','$scope','$uibModal','loginService', function(dataService,$interval,$scope,$uibModal,loginService){
    $scope.pageClass = 'page-forms';
	$scope.mtstxt = "Leave Form application section";
	$scope.logout = function() {
        var uemail_log = $('.uemail_log').val();
		loginService.logout(uemail_log);
	};

    dataService.getleave(function(response){
        $scope.leave = response.data;
    });

    // $interval(function(){
    //     // call leaves in interval
    //     dataService.getleave(function(response){
    //         $scope.leave = response.data;
    //     });
    // }, 5000);

	dataService.getLoggedinUser(function(response){
	    $scope.logged = response.data;
	});
	$scope.leav_apply = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/views/leave_apply.php',
            controller: 'leaveDataCall',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.profileView = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/personalDetails.php',
            controller: 'personalData',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.settings = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/settings.php',
            controller: 'settingCtrl',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.view_applied_leave = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/views/view_leave.php',
            controller: 'viewLeaveData',
            size: size,
            resolve: {
                leave: function () {
                    return p;
                }
            }
        });
    };
    $scope.respond_to_leave = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/views/respond_to_leave.php',
            controller: 'viewLeaveData',
            size: size,
            resolve: {
                leave: function () {
                    return p;
                }
            }
        });
    };
}]);
angular.module('Felcettiapp').controller('viewLeaveData', ['$scope', '$http', '$uibModalInstance', 'leave', 'dataService',function ($scope, $http, $uibModalInstance, leave, dataService) {
    $scope.leave = angular.copy(leave);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.respond_leave = function () {
        var lid = $('.leave_id').val();
        var leave_state = $('.leave_reply').val();
        var leave_comment = $('.leave_comment').val();
        if (lid == "" || leave_state == "" || leave_comment == "") {
            swal('Error!', 'Fill all fields', 'error');
        }else {
            $http.post("../assets/ajax/leave_update.php",{'lid':lid,'leave_state':leave_state,'leave_comment':leave_comment}).success(function(data){
                if(data.status == "success"){
                    swal('Updated!', data.message, 'success');
                }else{
                  swal('Error', data.message, 'error');
                }
            });
        }
    };
}]);


angular.module('Felcettiapp').controller('leaveDataCall', ['$scope', '$http', '$uibModalInstance', 'log', 'dataService',function ($scope, $http, $uibModalInstance, log, dataService) {
    $scope.log = angular.copy(log);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.postLeaveData = function () {
        var names = $('.names').val();
        var department = $('.department').val();
        var id = $('.uid').val();
        var phonenumber = $('.phonenumber').val();
        var usermail = $('.usermail').val();
        var kin = $('.kin').val();
        var kinphone = $('.kinphone').val();
        var nodays = $('.nodays').val();
        var datefrom = $('.datefrom').val();
        var dateto = $('.dateto').val();
        var relievername = $('.relievername').val();
        var relieverdepart = $('.relieverdepart').val();

        if (names == "" || department == "" || id == "" || phonenumber == "" || usermail == "" || kin == "" || kinphone == "" || nodays == "" || datefrom == "" || dateto == "" || relievername == "" || relieverdepart == "") {
            swal('Error', 'Please fill all records', 'error');
        }
        else {
            swal({ title: "Please wait!", showConfirmButton: false });
            $http.post("../assets/ajax/leave_apply.php",{'uid':id,'names':names,'department':department,'phonenumber':phonenumber,'usermail':usermail,'kin':kin,'kinphone':kinphone,'nodays':nodays,'datefrom':datefrom,'dateto':dateto,'relievername':relievername,'relieverdepart':relieverdepart}).success(function(data){
                if(data.status == "success"){
                    swal('Applied!', data.message, 'success');
                    $('.nodays').val("");
                    $('.datefrom').val("");
                    $('.dateto').val("");
                    $('.relievername').val("");
                    $('.relieverdepart').val("");
                    // $('.closeleave').trigger(click);
                    // $scope.leave.push({ name: names, department: department, relievername: relievername});
                }else{
                  swal('Error', data.message, 'error');
                }
            });
        }
    };
}]);