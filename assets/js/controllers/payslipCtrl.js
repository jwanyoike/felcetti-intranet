'use strict';
angular.module('Felcettiapp').controller('payslipCtrl', ['dataService','$scope','$uibModal','loginService', function(dataService,$scope,$uibModal,loginService){
    $scope.pageClass = 'page-payslip';
	$scope.mtstxt = "Personal payslip";
	$scope.logout = function() {
		var uemail_log = $('.uemail_log').val();
        loginService.logout(uemail_log);
	};
    dataService.getpays(function(response){
        $scope.pays = response.data;
    });
	dataService.getLoggedinUser(function(response){
	    $scope.logged = response.data;
	});
    $scope.view_payslip = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/views/view_payslip.php',
            controller: 'viewMonthPay',
            size: size,
            resolve: {
                pay: function () {
                    return p;
                }
            }
        });
    };
    $scope.addSlip = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/views/new_payslip.php',
            controller: 'createPayslip',
            size: size,
            resolve: {
                pay: function () {
                    return p;
                }
            }
        });
    };
    $scope.profileView = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/personalDetails.php',
            controller: 'personalData',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    $scope.settings = function(p,size){
        var modalInstance = $uibModal.open({
            templateUrl: '../assets/templates/users/settings.php',
            controller: 'settingCtrl',
            size: size,
            resolve: {
                log: function () {
                    return p;
                }
            }
        });
    };
    
}]);
angular.module('Felcettiapp').controller('viewMonthPay', ['$scope', '$http', '$uibModalInstance', 'pay', 'dataService',function ($scope, $http, $uibModalInstance, pay, dataService) {
    $scope.pay = angular.copy(pay);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
angular.module('Felcettiapp').controller('createPayslip', ['$scope', '$http', '$uibModalInstance', 'dataService',function ($scope, $http, $uibModalInstance, dataService) {
    dataService.getUsers(function(response){
        $scope.users = response.data;
    });
    // $scope.pay = angular.copy(pay);
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.Add_sal = function () {
        var emp_name = $( ".emp_name option:selected" ).text();
        var emp_depart = $( ".emp_depart option:selected" ).text();
        var emp_email = $( ".emp_email option:selected" ).text();
        var emp_id = $( ".emp_id" ).val();
        var sal_month = $scope.sal_month;
        var basic = $scope.basic;
        var other_ben = $scope.other_ben;
        var gross_sal = $( ".gross_sal" ).val();
        var paye = $scope.paye;
        var p_relief = $scope.p_relief;
        var paye_bal = $( ".paye_bal" ).val();
        var nssf = $scope.nssf;
        var nhif = $scope.nhif;
        var total_deduct = $( ".total_deduct" ).val();
        var net_pay = $( ".net_pay" ).val();

        if (emp_name == "" || emp_depart == "" || emp_email == "" || emp_id == "" || sal_month == null || basic == null || other_ben == null || gross_sal == "" || paye == null || p_relief == null || nssf == null || nhif == null || total_deduct == "" || net_pay == "") {
             swal('Error','fill all blanks', 'error');
        }else {
            $http.post("../assets/ajax/add_salary.php",{'emp_name':emp_name,'emp_depart':emp_depart,'emp_email':emp_email,'emp_id':emp_id,'sal_month':sal_month,'basic':basic,'other_ben':other_ben,'gross_sal':gross_sal,'paye':paye,'p_relief':p_relief,'paye_bal':paye_bal,'nssf':nssf,'nhif':nhif,'total_deduct':total_deduct,'net_pay':net_pay}).success(function(data){
                if(data.status == "success"){
                    swal('Paid!', data.message, 'success');
                    $(".emp_name").text("");
                }else{
                  swal('Error', data.message, 'error');
                }
            });
        }
    };
}]);