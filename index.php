
<!DOCTYPE html>
<html ng-app="Felcettiapp" lang="en">
<head>
    <meta charset="UTF-8">
    <title>Felcetti Intranet</title>
    <link rel="shortcut icon" href="assets/images/favicon_1.ico">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="assets/js/sweetalert/dist/sweetalert2.min.css" rel="stylesheet" type="text/css">

    <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- <script src="assets/js/modernizr.min.js"></script> -->
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet"> -->
    <script src="assets/js/jquery-2.1.3.min.js"></script>
    <script src="assets/js/angular.js"></script>
    <script src="assets/js/filters/ui-bootstrap-tpls-0.14.3.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/routes.js"></script>
    <script src="assets/js/angular-animate.min.js"></script>
    <!-- <script src="assets/js/controllers/IndexController.js"></script> -->

    <script src="assets/js/controllers/loginCtrl.js"></script>
    <script src="assets/js/controllers/homeCtrl.js"></script>
    <script src="assets/js/controllers/formCtrl.js"></script>
    <script src="assets/js/controllers/pernalDataCtrl.js"></script>
    <script src="assets/js/controllers/payslipCtrl.js"></script>
    <script src="assets/js/controllers/settings.js"></script>

    <script src="assets/js/services/loginService.js"></script>
    <script src="assets/js/services/sessionService.js"></script>

    <script src="assets/js/jquery.form.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js" type="text/javascript"></script>

    <style type="text/css">
      body{
        background-color: transparent;
      }
    </style>
    

</head>
<body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a ng-href="#/home" class="logo"><i class="fa-letter-F icon-c-logo"></i><span>fel<i class="fa fa-letter-CE"></i>tti</span></a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left">
                                    <i class="ion-navicon"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>



                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown hidden-xs">
                                    <div data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                                    </div>
                                    <ul class="dropdown-menu dropdown-menu-lg">
                                        <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
                                        <li class="list-group nicescroll notification-list">
                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-diamond fa-2x text-primary"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-cog fa-2x text-custom"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">New settings</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-bell-o fa-2x text-danger"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">Updates</h5>
                                                    <p class="m-0">
                                                        <small>There are <span class="text-primary font-600">2</span> new updates available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-user-plus fa-2x text-info"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">New user registered</h5>
                                                    <p class="m-0">
                                                        <small>You have 10 unread messages</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                           <a href="javascript:void(0);" class="list-group-item">
                                              <div class="media">
                                                 <div class="pull-left p-r-10">
                                                    <em class="fa fa-diamond fa-2x text-primary"></em>
                                                 </div>
                                                 <div class="media-body">
                                                    <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                                    <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                 </div>
                                              </div>
                                           </a>

                                           <!-- list item-->
                                            <a href="javascript:void(0);" class="list-group-item">
                                                <div class="media">
                                                    <div class="pull-left p-r-10">
                                                     <em class="fa fa-cog fa-2x text-custom"></em>
                                                    </div>
                                                    <div class="media-body">
                                                      <h5 class="media-heading">New settings</h5>
                                                      <p class="m-0">
                                                        <small>There are new settings available</small>
                                                    </p>
                                                    </div>
                                              </div>
                                           </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="list-group-item text-right">
                                                <small class="font-600">See all notifications</small>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hidden-xs">
                                    <div id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></div>
                                </li>
                                <li class="hidden-xs">
                                    <div class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></div>
                                </li>
                                <li class="dropdown">
                                    <div class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle" style="visibility: hidden;"> </div>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
                        
                <div class="left side-menu">
                    <div class="sidebar-inner slimscrollleft">
                        <div id="sidebar-menu">
                            <ul>

                              <li class="text-muted menu-title">Navigation</li>

                              <li class="has_sub">
                                  <a href="#home" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>
                              </li>

                              <li class="has_sub">
                                  <a href="#forms" class="waves-effect"><i class="ti-pencil-alt"></i><span> Forms </span></a>
                              </li>

                              <li class="has_sub">
                                  <a href="#payslip" class="waves-effect"><i class="ti-menu-alt"></i><span>Payslip </span></a>
                              </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
            <!-- ========== Left Sidebar Start ========== -->
                      <!-- <form action="upload.php" class="dropzone" id="my-awesome-dropzone"></form> -->
                      <div class="page {{ pageClass }}" ng-view></div>
                      <!-- <span ng-view></span> -->
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Felcetti.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->
    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    
    <script src="assets/js/angular-route.js"></script>
    <script src="assets/js/angular-resource.js"></script>
    <!-- <script src="assets/js/bootstrap.js"></script> -->
    <script src="assets/js/filters/angular-timestamp-filter.min.js"></script>

    <script src="assets/js/sweetalert/dist/sweetalert2.min.js"></script>

    <!-- Directives -->
    <!-- <script src="assets/javascript/directives/nwPageNav.js"></script>
    <script src="assets/javascript/directives/nwCategorySelector.js"></script>
    <script src="assets/javascript/directives/title.js"></script> -->

    <!-- Controllers -->
    <!-- <script src="assets/js/controllers/noteIndexController.js"></script> -->
    <script src="assets/js/controllers/usersShowController.js"></script>
    <!-- <script src="assets/js/controllers/noteShowController.js"></script>
    <script src="assets/js/controllers/noteEditController.js"></script>
    <script src="assets/js/controllers/noteCreateController.js"></script>-->

    <!-- Filters -->
    <!-- <script src="assets/javascript/filters/categoryFilter.js"></script> -->

    <!-- Services -->
    <script src="assets/js/services/user.js"></script>
    <!-- <script src="assets/javascript/services/note.js"></script>

    <script src="assets/javascript/services/category.js"></script>
-->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>

    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <script src="assets/plugins/peity/jquery.peity.min.js"></script>

    <!-- jQuery  -->
    <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
    <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>



    <script src="assets/plugins/morris/morris.min.js"></script>
    <script src="assets/plugins/raphael/raphael-min.js"></script>

    <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

    <script src="assets/pages/jquery.dashboard.js"></script>

    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>
    <script src="assets/plugins/moment/moment.js"></script>
    <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });
            $(".knob").knob();
        });
    </script>
    
</body>
</html>
